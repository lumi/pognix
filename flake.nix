{
  inputs = {
    naersk.url = "github:nix-community/naersk/master";
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils, naersk }:
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        naersk-lib = pkgs.callPackage naersk { };
        pognixPkg = naersk-lib.buildPackage rec {
          pname = "pognix";

          src = ./.;

          nativeBuildInputs = with pkgs; [ makeWrapper rustPlatform.bindgenHook ];

          buildInputs = with pkgs; [ pam ];

          postInstall = ''
            cp -r ${./data} $out/data
            cp flake.nix flake.lock $out
            wrapProgram $out/bin/${pname} \
              --prefix PATH : ${pkgs.lib.makeBinPath (with pkgs; [ podman nix ])} \
              --prefix POGNIX_FLAKE : $out \
              --prefix POGNIX_LOGIN : $out/bin/pognix-login
          '';
        };
        pognixApp = utils.lib.mkApp {
          drv = pognixPkg;
        };
      in
      {
        packages.pognix = pognixPkg;
        packages.default = pognixPkg;

        apps.pognix = pognixApp;
        apps.default = pognixApp;

        devShell = with pkgs; mkShell {
          nativeBuildInputs = [ rustPlatform.bindgenHook ];
          buildInputs = [ cargo rustc rustfmt pre-commit rustPackages.clippy pam rust-analyzer ];
          RUST_SRC_PATH = rustPlatform.rustLibSrc;
        };
      })
    // {
      nixosModules.pognix-host = import ./data/pognix-host.nix { pognix = self; };
      nixosModules.pognix-guest = import ./data/pognix-guest.nix;
    };
}
