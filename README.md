# Pognix

## NOTICE

Pognix is currently undergoing big changes, so documentation and examples might be out of date!

## Intro

Pognix is a tool for setting up ephemeral development containers using Podman and Nix.

It combines a regular NixOS system definition with whatever is added to it in the `flake.nix` of a project.

## Installation

Add an input to your NixOS `flake.nix` adding pognix:

```nix
{
  inputs = {
    # ...
    pognix.url = "git+https://codeberg.org/lumi/pognix.git";
    # ...
  };
}
```

Then you can add `pognix.nixosModules.pognix-host` to your NixOS system modules and configure it:

```nix
{
  imports = [
    pognix.nixosModules.pognix-host
  ];

  virtualisation.pognix = {
    enable = true;
    systemFlake = "/path/to/dev/system/flake#devSystemName";
  };
}
```

Here, `"path/to/dev/system/flake"` is a path to where the flake containing the desired development NixOS system is and `"devSystemName"` is the name of this system.

`systemFlake` defaults to `"flake:nixos-config#devel"`.


## Usage

When you're within a project with a `flake.nix`, you can run `pognix enter --start --stop-on-exit` to build and start a development container.

This container will exit when the session closes, you may remove the `--stop-on-exit` option to prevent this.

A project itself can configure the NixOS system by adding `pognix.extraModules` to its outputs, this is a list of modules to add to the system.

Pognix itself has a few options to configure it, as well.

There are some examples in the `examples` directory of this repository.

## License

This project is licensed under the GNU Affero General Publice License version 3 or later.
