{ pognix }:

{ config, pkgs, lib, ... }:

let
  cfg = config.virtualisation.pognix-host;
in {
  options = {
    virtualisation.pognix-host = with lib; {
      enable = mkEnableOption "Pognix host";

      systemFlake = mkOption {
        type = types.str;
        default = "flake:nixos-config#devel";
      };
    };
  };

  config = lib.mkIf cfg.enable {
    environment.systemPackages = [ pognix.packages.${pkgs.system}.pognix ];

    environment.etc."pognix.toml" = {
      text = ''
        [system]
        flake = "${cfg.systemFlake}"
      '';
    };
  };
}
