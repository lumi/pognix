{ config, pkgs, lib, ... }:

let
  cfg = config.pognix;

  volumeSubmodule = with lib; types.submodule ({ name, ... }: {
    options = {
      name = mkOption {
        type = types.str;
      };
      context = mkOption {
        type = types.nullOr types.str;
        default = null;
      };
      dest = mkOption {
        type = types.str;
      };
    };
    config = {
      name = mkDefault name;
    };
  });
in {
  options = {
    pognix = with lib; {
      container = {
        user = lib.mkOption {
          type = types.str;
        };
        uid = lib.mkOption {
          type = types.int;
        };
      };
      volumes = lib.mkOption {
        type = types.attrsOf volumeSubmodule;
        default = {};
      };
      network = {
        enable = lib.mkOption {
          type = types.bool;
          default = true;
        };
        mappings = lib.mkOption {
          type = types.listOf types.str;
          default = [];
        };
      };
      expose = {
        wayland = lib.mkOption {
          type = types.bool;
          default = false;
        };
        x11 = lib.mkOption {
          type = types.bool;
          default = false;
        };
        pulse = lib.mkOption {
          type = types.bool;
          default = false;
        };
        pipewire = lib.mkOption {
          type = types.bool;
          default = false;
        };
        input = lib.mkOption {
          type = types.bool;
          default = false;
        };
        gpu = lib.mkOption {
          type = types.bool;
          default = false;
        };
        xdg-open = lib.mkOption {
          type = types.bool;
          default = false;
        };
      };
    };
  };

  config = let
    normalUsers = lib.filter (u: u.isNormalUser) (lib.attrValues config.users.users);
    detectedUser = lib.head normalUsers;
    configFile = pkgs.writeTextFile { name = "pognix.json"; text = (builtins.toJSON cfg); };
    hostInitScript = pkgs.writeScript "host-init.sh" ''
      echo '{"type": "ready"}' | ${pkgs.netcat}/bin/nc -NU /host/control
    '';
    hostUserInitScript = pkgs.writeScript "host-user-init.sh" ''
      ${if cfg.expose.wayland then ''
        ln -s /host/run/wayland-1 $XDG_RUNTIME_DIR/wayland-1
      '' else ""}
      ${if cfg.expose.pulse then ''
        mkdir -p $XDG_RUNTIME_DIR/pulse
        ln -s /host/run/pulse/native $XDG_RUNTIME_DIR/pulse/native
      '' else ""}
      ${if cfg.expose.pipewire then ''
        ln -s /host/run/pipewire-0 $XDG_RUNTIME_DIR/pipewire-0
      '' else ""}
    '';
    new-xdg-open = pkgs.writeScript "xdg-open" ''
      #!/bin/sh
      echo $1 | ${pkgs.jq}/bin/jq -Rc '{"type": "open", "url": .}' | nc -NU /host/control >/dev/null
    '';
  in {
    boot.isContainer = true;

    nixpkgs.overlays = [
      (_: super: {
        xdg-utils = super.xdg-utils.overrideAttrs (oldAttrs: {
          postFixup = ''
            rm $out/bin/xdg-open
            cp ${new-xdg-open} $out/bin/xdg-open
            chmod +x $out/bin/xdg-open
          '';
        });
      })
    ];

    system.extraSystemBuilderCmds = ''
      cp ${configFile} $out/pognix.json
    '';

    pognix.container.user = lib.mkDefault detectedUser.name;
    pognix.container.uid = lib.mkDefault detectedUser.uid;

    system.activationScripts.setup-home.text = lib.concatMapStringsSep "\n" (u: let user = u.name; in ''
      mkdir -p /nix/var/nix/profiles/per-user/${user}
      chown user:users /nix/var/nix/profiles/per-user/${user}
      mkdir -p /nix/var/nix/gcroots/per-user/${user}
      chown user:users /nix/var/nix/gcroots/per-user/${user}
    '') normalUsers;

    system.activationScripts.chown-rootfs = ''
      chown root:root /
    '';

    systemd.user.services.host-user-init = {
      wantedBy = [ "default.target" ];
      partOf = [ "default.target" ];
      serviceConfig = {
        Type = "oneshot";
        ExecStart = "/bin/sh ${hostUserInitScript}";
      };
    };

    systemd.services.host-init = {
      wantedBy = [ "multi-user.target" ];
      partOf = [ "multi-user.target" ];
      serviceConfig = {
        Type = "oneshot";
        ExecStart = "/bin/sh ${hostInitScript}";
      };
    };

    # These don't work in the container
    systemd.services.systemd-oomd.enable = lib.mkForce false;
    systemd.services.nscd.enable = lib.mkForce false;
    systemd.services.logrotate-checkconf.enable = lib.mkForce false;
    systemd.sockets.systemd-oomd.enable = lib.mkForce false;
    systemd.sockets.nix-daemon.enable = lib.mkForce false;

    environment.variables =
      lib.optionalAttrs cfg.expose.wayland { WAYLAND_DISPLAY = "wayland-1"; } //
      lib.optionalAttrs cfg.expose.x11 { DISPLAY = ":0"; };
  };
}
