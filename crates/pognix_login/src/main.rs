use std::{os::unix::process::CommandExt, path::PathBuf, process::Command};

use clap::Parser;
use nix::unistd::User;
use pam_client::{conv_null::Conversation, Context, Flag};

#[derive(Parser)]
pub struct Opt {
    pub user: String,
    pub dir: PathBuf,
}

fn main() -> anyhow::Result<()> {
    let opt = Opt::from_args();
    let user = User::from_name(&opt.user)?
        .ok_or_else(|| anyhow::anyhow!("user {:?} not found", opt.user))?;
    let mut ctx = Context::new("login", Some(&opt.user), Conversation::new())?;
    ctx.acct_mgmt(Flag::NONE)?;
    let session = ctx.open_session(Flag::NONE)?;
    let term = std::env::var("TERM")?;
    Command::new(&user.shell)
        .arg("--login")
        .env_clear()
        .envs(session.envlist().iter_tuples())
        .env("TERM", &term)
        .uid(user.uid.as_raw())
        .gid(user.gid.as_raw())
        .current_dir(&opt.dir)
        .status()?;
    let _ = session;
    Ok(())
}
