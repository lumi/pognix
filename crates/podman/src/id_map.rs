/// Type representing mappings from ids to other ids
///
/// Used with uid and gid mappings
#[derive(Debug, Clone)]
pub struct IdMap {
    /// Concrete mappings in the format of uidmap and gidmap
    pub mappings: Vec<(u32, u32, u32)>,
}

impl IdMap {
    /// Maximum allowed id
    pub const ID_MAX: u32 = 65536;

    /// Create an empty id map
    pub fn new() -> IdMap {
        IdMap {
            mappings: Vec::new(),
        }
    }

    /// Create an id map that maps:
    ///
    ///  - `== id` to `0`
    ///  - `< id` to `id + 1`
    ///  - `> id` to itself
    pub fn passthrough(id: u32) -> IdMap {
        let mut map = IdMap::new();
        map.add(0, 1, id - 1);
        map.add(id, 0, 1);
        map.add(id + 1, id + 1, Self::ID_MAX - id - 1);
        map
    }

    /// Add a mapping to the map
    pub fn add(&mut self, host: u32, intermediate: u32, count: u32) {
        self.mappings.push((host, intermediate, count));
    }

    /// Get the map as a list of strings
    pub fn to_strings(&self) -> impl Iterator<Item = String> + '_ {
        self.mappings
            .iter()
            .map(|(host, intermediate, count)| format!("{}:{}:{}", host, intermediate, count))
    }
}
