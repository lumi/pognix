use std::{
    io::{prelude::*, BufReader},
    process::{Command, Stdio},
};

mod exec;
mod id_map;
mod network;
mod run;
mod volume;

pub use exec::ExecBuilder;
pub use id_map::IdMap;
pub use network::PortMapping;
pub use run::RunBuilder;
pub use volume::{Volume, VolumeOpts};

/// List all running containers
pub fn ps() -> anyhow::Result<Vec<String>> {
    log::debug!("getting running containers");
    let mut cmd = Command::new("podman");
    cmd.arg("ps");
    cmd.arg("-a");
    cmd.arg("--format={{ .Names }}");
    cmd.stdout(Stdio::piped());
    let mut child = cmd.spawn()?;
    let stdout = BufReader::new(child.stdout.take().unwrap());
    let mut out = Vec::new();
    for line in stdout.lines() {
        log::trace!("found container {line:?}");
        let line = line?;
        out.push(line);
    }
    let code = child.wait()?;
    if code.success() {
        log::debug!("found {} containers", out.len());
        Ok(out)
    } else {
        log::error!("failed to get running containers");
        Err(anyhow::anyhow!("failed to get running containers"))
    }
}

/// Start a new container
pub fn run() -> RunBuilder {
    RunBuilder::new()
}

/// Run a command in an existing container
pub fn exec(name: &str) -> ExecBuilder {
    ExecBuilder::new(name)
}

/// Kill a container
pub fn stop(name: &str) -> anyhow::Result<()> {
    log::debug!("stopping container {name:?}");
    let mut cmd = Command::new("podman");
    cmd.arg("rm");
    cmd.arg("-f");
    cmd.arg(name);
    cmd.stdout(Stdio::piped());
    let mut child = cmd.spawn()?;
    let mut stdout = child.stdout.take().unwrap();
    let mut output = String::new();
    stdout.read_to_string(&mut output)?;
    let code = child.wait()?;
    if code.success() {
        log::debug!("container stopped");
        let _container_id = output.trim(); // TODO: maybe return this?
        Ok(())
    } else {
        log::error!("failed to stop container {name:?}");
        Err(anyhow::anyhow!("failed to stop container"))
    }
}
