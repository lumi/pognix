use std::{
    io::prelude::*,
    path::{Path, PathBuf},
    process::{Command, Stdio},
};

use super::{IdMap, PortMapping, Volume, VolumeOpts};

pub struct RunBuilder {
    interactive: bool,
    name: Option<String>,
    uidmap: Option<IdMap>,
    gidmap: Option<IdMap>,
    rootfs: Option<PathBuf>,
    tmpfs: Vec<PathBuf>,
    volumes: Vec<Volume>,
    port_mappings: Vec<PortMapping>,
    disable_net: bool,
}

impl RunBuilder {
    pub fn new() -> RunBuilder {
        RunBuilder {
            interactive: false,
            name: None,
            uidmap: None,
            gidmap: None,
            rootfs: None,
            tmpfs: Vec::new(),
            volumes: Vec::new(),
            port_mappings: Vec::new(),
            disable_net: false,
        }
    }

    pub fn interactive(&mut self) {
        self.interactive = true;
    }

    pub fn name(&mut self, name: &str) {
        self.name = Some(name.to_owned());
    }

    pub fn uidmap(&mut self, mapping: IdMap) {
        self.uidmap = Some(mapping);
    }

    pub fn gidmap(&mut self, mapping: IdMap) {
        self.gidmap = Some(mapping);
    }

    pub fn rootfs(&mut self, path: impl AsRef<Path>) {
        self.rootfs = Some(path.as_ref().to_owned());
    }

    pub fn tmpfs(&mut self, path: impl AsRef<Path>) {
        self.tmpfs.push(path.as_ref().to_owned());
    }

    pub fn bind(&mut self, host: impl AsRef<Path>, guest: impl AsRef<Path>, opts: VolumeOpts) {
        self.volumes.push(Volume::new(host, guest, opts));
    }

    pub fn bind_ro(&mut self, host: impl AsRef<Path>, guest: impl AsRef<Path>) {
        self.bind(host, guest, VolumeOpts::Ro);
    }

    pub fn bind_rw(&mut self, host: impl AsRef<Path>, guest: impl AsRef<Path>) {
        self.bind(host, guest, VolumeOpts::Rw);
    }

    pub fn bind_dev(&mut self, host: impl AsRef<Path>, guest: impl AsRef<Path>) {
        self.bind(host, guest, VolumeOpts::Dev);
    }

    pub fn port_mapping(&mut self, mapping: &str) {
        self.port_mappings.push(PortMapping(mapping.to_owned()));
    }

    pub fn disable_net(&mut self) {
        self.disable_net = true;
    }

    pub fn run(self, command: &str) -> anyhow::Result<Option<String>> {
        log::debug!("setting up podman run command");
        let mut cmd = Command::new("podman");
        cmd.arg("run");
        cmd.arg("--rm");
        if self.interactive {
            log::debug!("setting interactive");
            cmd.arg("-t");
            cmd.arg("-i");
        } else {
            log::debug!("setting background");
            cmd.arg("-d");
        }
        if let Some(name) = self.name {
            log::debug!("setting name to {name:?}");
            cmd.arg("--name");
            cmd.arg(name);
        }
        if let Some(uidmap) = self.uidmap {
            log::debug!("setting uid mappings");
            for mapping in uidmap.to_strings() {
                log::debug!("setting uid mapping {mapping:?}");
                cmd.arg("--uidmap");
                cmd.arg(mapping);
            }
        }
        if let Some(gidmap) = self.gidmap {
            log::debug!("setting gid mappings");
            for mapping in gidmap.to_strings() {
                log::debug!("setting gid mapping {mapping:?}");
                cmd.arg("--gidmap");
                cmd.arg(mapping);
            }
        }
        for path in self.tmpfs {
            log::debug!("mounting tmpfs on {path:?}");
            cmd.arg("--tmpfs");
            cmd.arg(path);
        }
        for vol in self.volumes {
            let vol_string = vol.to_string();
            log::debug!("mounting volume {vol_string:?}");
            cmd.arg("-v");
            cmd.arg(vol_string);
        }
        for mapping in self.port_mappings {
            log::debug!("adding port mapping {:?}", mapping.0);
            cmd.arg("-p");
            cmd.arg(mapping.0);
        }
        if self.disable_net {
            log::debug!("disabling net");
            cmd.arg("--net");
            cmd.arg("none");
        }
        cmd.arg("--systemd=always");
        if let Some(rootfs) = self.rootfs {
            log::debug!("adding rootfs {rootfs:?}");
            cmd.arg("--rootfs");
            cmd.arg(rootfs);
        }
        log::debug!("adding command {command:?}");
        cmd.arg(command);
        if self.interactive {
            log::debug!("running interactively");
            let mut child = cmd.spawn()?;
            let code = child.wait()?;
            if code.success() {
                Ok(None)
            } else {
                Err(anyhow::anyhow!("failed to start container"))
            }
        } else {
            log::debug!("running non-interactively");
            cmd.stdout(Stdio::piped());
            let mut child = cmd.spawn()?;
            let mut stdout = child.stdout.take().unwrap();
            let mut out = String::new();
            stdout.read_to_string(&mut out)?;
            let code = child.wait()?;
            if code.success() {
                Ok(Some(out.trim().to_owned()))
            } else {
                Err(anyhow::anyhow!("failed to start container"))
            }
        }
    }
}
