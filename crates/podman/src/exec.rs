use std::process::Command;

pub struct ExecBuilder {
    name: String,
    env: Vec<(String, String)>,
    user: Option<String>,
}

impl ExecBuilder {
    pub fn new(name: &str) -> ExecBuilder {
        ExecBuilder {
            name: name.to_owned(),
            env: Vec::new(),
            user: None,
        }
    }

    pub fn env(&mut self, var: &str, value: &str) {
        self.env.push((var.to_owned(), value.to_owned()));
    }

    pub fn user(&mut self, user: &str) {
        self.user = Some(user.to_owned());
    }

    pub fn exec(self, command: &[&str]) -> anyhow::Result<()> {
        log::debug!("setting up podman exec command to run {command:?}");
        let mut cmd = Command::new("podman");
        cmd.arg("exec");
        cmd.arg("-t");
        cmd.arg("-i");
        for (var, value) in &self.env {
            log::debug!("adding env var {var:?}={value:?}");
            cmd.arg("-e");
            cmd.arg(format!("{}={}", var, value));
        }
        if let Some(user) = &self.user {
            log::debug!("running as user {user:?}");
            cmd.arg("--user");
            cmd.arg(user);
        }
        log::debug!("in container {:?}", self.name);
        cmd.arg(&self.name);
        for arg in command {
            cmd.arg(arg);
        }
        let mut child = cmd.spawn()?;
        child.wait()?;
        Ok(())
    }
}
