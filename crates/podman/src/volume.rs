use std::path::{Path, PathBuf};

/// Options for mounting a volume
#[derive(Debug, Clone, Copy)]
pub enum VolumeOpts {
    /// Read-write
    Rw,
    /// Read-only
    Ro,
    /// Device
    Dev,
}

/// A volume to be mounted into the container
#[derive(Debug, Clone)]
pub struct Volume {
    /// Patho nn the host
    pub host: PathBuf,
    /// Path on the guest
    pub guest: PathBuf,
    /// Volume mounting options
    pub opts: VolumeOpts,
}

impl Volume {
    /// Create a new volume
    pub fn new(host: impl AsRef<Path>, guest: impl AsRef<Path>, opts: VolumeOpts) -> Self {
        Self {
            host: host.as_ref().to_owned(),
            guest: guest.as_ref().to_owned(),
            opts,
        }
    }

    /// Convert this volume into a string that is understood by podman
    pub fn to_string(&self) -> String {
        let host = self.host.display();
        let guest = self.guest.display();
        match self.opts {
            VolumeOpts::Rw => format!("{}:{}", host, guest),
            VolumeOpts::Ro => format!("{}:{}:ro", host, guest),
            VolumeOpts::Dev => format!("{}:{}:dev", host, guest),
        }
    }
}
