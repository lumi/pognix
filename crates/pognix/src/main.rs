use clap::Parser;

use pognix::{
    env::Env,
    opt::{Command, Opt},
    project::Project,
};

fn main() -> anyhow::Result<()> {
    pretty_env_logger::init();
    let opt = Opt::from_args();
    let env = Env::build(&opt)?;
    let project = Project::from_env(env)?;
    match opt.command {
        Command::Build {} => {
            log::info!("build command invokved");
            project.build()?;
        }
        Command::Start { interactive } => {
            log::info!("start command invoked with interactive={interactive:?}");
            if project.is_started()? {
                log::info!("project is running");
                eprintln!("container already started");
            } else {
                log::info!("project is not running");
                project.start(interactive)?;
            }
        }
        Command::Stop {} => {
            log::info!("stop command invoked");
            if project.is_started()? {
                log::info!("project is running");
                project.stop()?;
            } else {
                log::info!("project is not running");
                eprintln!("container not running");
            }
        }
        Command::Enter {
            user,
            root,
            start,
            stop_on_exit,
        } => {
            log::info!("enter command invoked with user={user:?}, root={root:?}, start={start:?}, stop_on_exit={stop_on_exit:?}");
            let cmd_user = if root { Some("root".to_owned()) } else { user };
            if project.is_started()? {
                log::info!("project is running");
                project.enter(cmd_user.as_deref())?;
                if stop_on_exit {
                    project.stop()?;
                }
            } else if start {
                log::info!("project is not running, told to start");
                project.start(false)?;
                project.enter(cmd_user.as_deref())?;
                if stop_on_exit {
                    project.stop()?;
                }
            } else {
                log::info!("project is not running, not told to start");
                eprintln!("container not started");
            }
        }
        Command::Run {
            user,
            root,
            command,
        } => {
            log::info!(
                "run command invoked with user={user:?}, root={root:?}, command={command:?}"
            );
            let cmd_user = if root { Some("root".to_owned()) } else { user };
            if project.is_started()? {
                log::info!("project is running");
                project.run(cmd_user.as_deref(), &command.join(" "))?;
            } else {
                log::info!("project is not running");
            }
        }
        Command::Clear {} => {
            log::info!("clear command invoked");
            if project.is_started()? {
                log::info!("project is running");
                eprintln!("container is running");
            } else {
                log::info!("project is not running");
                project.clear()?;
            }
        }
        Command::PrintConfig {} => {
            log::info!("print-config command invoked");
            let data = toml::to_string(&project.get_config()?)?;
            println!("{}", data);
        }
    }
    Ok(())
}
