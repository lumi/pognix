use std::{env, path::PathBuf};

use nix::unistd::{Gid, Uid};

use crate::{config::system::SystemConfig, opt::Opt, util};

/// Host system env
#[derive(Debug, Clone)]
pub struct SystemEnv {
    /// Path to `pognix-login`
    pub pognix_login: PathBuf,

    /// Pognix utility flake path
    pub pognix_flake: PathBuf,

    /// System pognix configuration
    pub config: SystemConfig,
}

/// Host env
#[derive(Debug, Clone)]
pub struct Env {
    /// Container name
    pub name: String,

    /// System environment
    pub system: SystemEnv,

    /// Current project root
    pub project_root: PathBuf,

    /// Pognix project flake path
    pub project_flake: PathBuf,

    /// TERM variable
    pub term: String,

    /// Host user id
    pub host_uid: Uid,

    /// Host group id
    pub host_gid: Gid,
}

impl Env {
    /// Build host env from opts and current environment
    pub fn build(opt: &Opt) -> anyhow::Result<Env> {
        let cwd = env::current_dir()?;

        log::debug!("cwd is {cwd:?}");

        let flake_root = if opt.project_flake.is_some() {
            cwd
        } else {
            util::find_flake_root(&cwd)?
        };

        log::debug!("flake root is {flake_root:?}");

        let project_root = opt
            .project_flake
            .clone()
            .map(|path| PathBuf::from(path))
            .unwrap_or_else(|| flake_root.clone());

        log::debug!("project root is {project_root:?}");

        let name = util::path_to_name(&flake_root);

        log::debug!("computed name is {name:?}");

        let pognix_login = PathBuf::from(env::var("POGNIX_LOGIN")?);
        let pognix_flake = PathBuf::from(env::var("POGNIX_FLAKE")?);

        log::debug!("pognix login executable is at {pognix_login:?}");
        log::debug!("pognix flake is at {pognix_flake:?}");

        let term = env::var("TERM")?;

        log::debug!("TERM is {term:?}");

        let host_uid = Uid::current();
        let host_gid = Gid::current();

        log::debug!("host uid={host_uid:?} gid={host_gid:?}");

        let mut config = if let Some(config_path) = &opt.config {
            log::debug!("loading system config from file {config_path:?}");
            SystemConfig::from_file(&config_path)?
        } else {
            log::debug!("loading system config from global");
            SystemConfig::from_global()?
        };

        if let Some(system_flake) = &opt.system_flake {
            log::debug!("overriding system flake to {system_flake:?}");
            config.system.flake = system_flake.to_owned();
        }

        Ok(Env {
            name,
            project_root: flake_root,
            project_flake: project_root,
            system: SystemEnv {
                pognix_login,
                pognix_flake,
                config,
            },
            term,
            host_uid,
            host_gid,
        })
    }
}
