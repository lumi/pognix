use handlebars::Handlebars;

use crate::{env::Env, util};

const TEMPLATE: &'static str = r#"
{
    inputs = {
        pognixFlake.url = "{{ pognix_flake_uri }}";
        systemFlake.url = "{{ system_flake_uri }}";
        projectFlake.url = "{{ project_flake_uri }}";
    };

    outputs = { pognixFlake, systemFlake, projectFlake, ... }: let
        nixosSystem = systemFlake.nixosConfigurations.{{ system_name }};
        pognixConfig = if builtins.hasAttr "pognix" projectFlake && builtins.hasAttr "extraModules" projectFlake.pognix
            then { extraModules = projectFlake.pognix.extraModules; }
            else { extraModules = []; };
    in {
        nixosConfigurations.pognix-dev = nixosSystem.extendModules {
            modules = [ pognixFlake.nixosModules.pognix-guest ] ++ pognixConfig.extraModules;
        };
    };
}
"#;

pub fn generate_flake(env: &Env) -> anyhow::Result<String> {
    let (system_flake, system_attr) = util::parse_system(&env.system.config.system.flake)?;

    let reg = Handlebars::new();

    let output = reg.render_template(
        TEMPLATE,
        &serde_json::json!({
            "pognix_flake_uri": &env.system.pognix_flake,
            "system_flake_uri": &system_flake,
            "system_name": &system_attr,
            "project_flake_uri": &env.project_flake,
        }),
    )?;

    Ok(output)
}
