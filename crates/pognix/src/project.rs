use std::{fs, os::unix::net::UnixListener, path::PathBuf};

use crate::{
    config::project::ProjectConfig,
    container::ContainerEnv,
    control::ControlSocket,
    env::Env,
    nix::{flake, Nix},
    paths::{ContainerPaths, ContainerVolume, ProjectPaths, UserPaths},
};

/// A pognix project
#[derive(Debug, Clone)]
pub struct Project {
    /// The env associated with this project
    pub env: Env,
    /// All the user paths of the current user
    pub user_paths: UserPaths,
    /// All the project paths for this project
    pub project_paths: ProjectPaths,
}

impl Project {
    /// Create a project from this env
    pub fn from_env(env: Env) -> anyhow::Result<Project> {
        let user_paths = UserPaths::ensure()?;
        let project_paths = user_paths.ensure_project(&env.name)?;
        Ok(Project {
            env,
            user_paths,
            project_paths,
        })
    }

    /// Get the configuration for this project
    pub fn get_config(&self) -> anyhow::Result<ProjectConfig> {
        let project_root = &self.project_paths.data_dir;
        let config_path = project_root.join("config.json");
        log::debug!("loading config from {config_path:?}");
        if config_path.is_file() {
            let text = fs::read_to_string(config_path)?;
            let config = serde_json::from_str(&text)?;
            return Ok(config);
        }
        anyhow::bail!("config file not found");
    }

    /// Get whether the current container is started
    pub fn is_started(&self) -> anyhow::Result<bool> {
        let ps = podman::ps()?;
        Ok(ps.iter().any(|p_name| p_name == &self.env.name))
    }

    /// Build the current container
    pub fn build(&self) -> anyhow::Result<()> {
        log::debug!("building top level");

        let project_root = &self.project_paths.data_dir;

        log::debug!("generating flake");

        let pognix_project_flake = project_root.join("flake.nix");
        fs::write(&pognix_project_flake, flake::generate_flake(&self.env)?)?;

        log::debug!("building toplevel");

        let toplevel = Nix::new().update_input("systemFlake").build(&format!(
            "{}#nixosConfigurations.pognix-dev.config.system.build.toplevel",
            self.project_paths.data_dir.display(),
        ))?;

        log::debug!("toplevel {toplevel:?}");

        log::debug!("creating toplevel link");

        crate::nix::add_gc_root(&toplevel, project_root.join("toplevel"))?;

        log::debug!("copying config");

        let toplevel_config = toplevel.join("pognix.json");
        let project_config = project_root.join("config.json");

        let _ = fs::remove_file(&project_config);

        fs::copy(&toplevel_config, &project_config)?;

        log::debug!("done");

        Ok(())
    }

    /// Clear built container
    pub fn clear(&self) -> anyhow::Result<()> {
        let project_root = &self.project_paths.data_dir;

        log::debug!("clearing {project_root:?}");

        fs::remove_dir_all(project_root)?;

        log::debug!("done");

        Ok(())
    }

    /// Start the current container, interactively if requested
    pub fn start(&self, interactive: bool) -> anyhow::Result<()> {
        println!("starting {}", self.env.name);

        log::info!("starting {}", self.env.name);

        // Load config
        log::debug!("loading config");

        let config = self.get_config()?;
        let toplevel = fs::read_link(&self.project_paths.data_dir.join("toplevel"))?;

        // Set up volumes
        log::debug!("setting up volumes");

        let mut volumes = Vec::new();

        for vol in config.volumes.values() {
            let context = vol.context.as_deref().unwrap_or(&self.env.name);
            let host_path = self.user_paths.ensure_volume(context, &vol.name)?;
            volumes.push(ContainerVolume {
                host: host_path.clone(),
                guest: PathBuf::from(&vol.dest),
            });
        }

        // Set up container paths
        log::debug!("setting up container paths");
        let paths = ContainerPaths::ensure(
            &self.user_paths,
            &self.project_paths,
            &volumes,
            &toplevel,
            &config.container.user,
        )?;

        // Create control socket
        log::debug!("setting up control socket on {:?}", paths.control);
        let control_listener = UnixListener::bind(&paths.control)?;

        // Set up container env
        log::debug!("setting up container env");
        let env = ContainerEnv::new(self.env.clone(), config.clone(), paths)?;

        // Set up run builder
        log::debug!("setting up podman builder");
        let mut start = podman::run();

        // Interactive
        if interactive {
            log::debug!("is interactive");
            start.interactive();
        }

        // Apply container env
        log::debug!("applying container env");
        env.apply(&mut start);

        // Run
        log::debug!("running container");
        start.run(&env.paths.toplevel.join("init").display().to_string())?;

        // Start control socket
        log::debug!("running control socket thread");
        ControlSocket::start(control_listener, !interactive, config.expose.clone())?;

        Ok(())
    }

    /// Stop the current container
    pub fn stop(&self) -> anyhow::Result<()> {
        println!("stopping {}", self.env.name);
        podman::stop(&self.env.name)?;
        Ok(())
    }

    /// Open a shell to the current container
    pub fn enter(&self, user: Option<&str>) -> anyhow::Result<()> {
        let config = self.get_config()?;
        let my_user = user.unwrap_or(&config.container.user);
        let mut exec = podman::exec(&self.env.name);
        exec.user("root");
        exec.env("TERM", &self.env.term);
        exec.exec(&[
            &self.env.system.pognix_login.display().to_string(),
            my_user,
            &self.env.project_root.display().to_string(),
        ])?;
        Ok(())
    }

    /// Run a command in the running container
    pub fn run(&self, user: Option<&str>, command: &str) -> anyhow::Result<()> {
        let config = self.get_config()?;
        let my_user = user.unwrap_or(&config.container.user);
        let mut exec = podman::exec(&self.env.name);
        exec.user(my_user);
        exec.env("TERM", &self.env.term);
        exec.exec(&["/bin/sh", "--login", "-c", command])?;
        Ok(())
    }
}
