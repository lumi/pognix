use std::{
    io::{prelude::*, BufReader},
    os::unix::net::{UnixListener, UnixStream},
    process::Command,
    thread,
};

use serde::{Deserialize, Serialize};

use crate::config::project::Expose;

#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all = "kebab-case", tag = "type")]
enum ControlRequest {
    Ready {},
    Open {
        url: String,
    },
    #[serde(other)]
    Unknown,
}

#[derive(Debug, Clone, Serialize)]
#[serde(rename_all = "kebab-case", tag = "type")]
enum ControlResponse {
    Ok {},
    Error { text: String },
}

pub struct ControlSocket {
    listener: UnixListener,
    expose: Expose,
    waiter: Option<oneshot::Sender<()>>,
}

impl ControlSocket {
    pub fn start(listener: UnixListener, wait: bool, expose: Expose) -> anyhow::Result<()> {
        let (wtx, wrx) = oneshot::channel();
        let waiter = if wait {
            Some(wtx)
        } else {
            wtx.send(())?;
            None
        };
        let me = ControlSocket {
            listener,
            expose,
            waiter,
        };
        thread::spawn(move || me.main());
        wrx.recv()?;
        Ok(())
    }

    fn main(mut self) {
        loop {
            let (sock, _addr) = self.listener.accept().unwrap(); // TODO: check unwrap
            match self.handle_socket(sock) {
                Ok(()) => (),
                Err(err) => {
                    eprintln!("control: error occured: {}", err);
                }
            }
        }
    }

    fn handle_socket(&mut self, mut sock: UnixStream) -> anyhow::Result<()> {
        let mut reader = BufReader::new(sock.try_clone()?);
        let mut line = String::new();
        loop {
            line.clear();
            let bytes_read = reader.read_line(&mut line)?;
            if bytes_read == 0 {
                break Ok(());
            }
            let response = match self.handle_line(&line) {
                Ok(response) => response,
                Err(_err) => ControlResponse::Error {
                    text: "generic".to_owned(),
                },
            };
            let mut response_text = serde_json::to_string(&response)?;
            response_text.push('\n');
            sock.write_all(response_text.as_bytes())?;
        }
    }

    fn handle_line(&mut self, line: &str) -> anyhow::Result<ControlResponse> {
        let request: ControlRequest = serde_json::from_str(line)?;
        let response = self.handle_request(request)?;
        Ok(response)
    }

    fn handle_request(&mut self, request: ControlRequest) -> anyhow::Result<ControlResponse> {
        match request {
            ControlRequest::Ready {} => {
                if let Some(waiter) = self.waiter.take() {
                    waiter.send(())?;
                }
                Ok(ControlResponse::Ok {})
            }
            ControlRequest::Open { url } => {
                if self.expose.xdg_open {
                    let mut child = Command::new("xdg-open").arg(&url).spawn()?;
                    thread::spawn(move || {
                        let _ = child.wait();
                    });
                    Ok(ControlResponse::Ok {})
                } else {
                    Ok(ControlResponse::Error {
                        text: "denied".to_owned(),
                    })
                }
            }
            ControlRequest::Unknown => Ok(ControlResponse::Error {
                text: "unknown request".to_owned(),
            }),
        }
    }
}
