use std::{
    io::prelude::*,
    path::{Path, PathBuf},
    process::{Command, Stdio},
};

pub mod flake;

pub struct Nix {
    file: Option<PathBuf>,
    impure: bool,
    args: Vec<(String, String)>,
    update_inputs: Vec<String>,
}

impl Nix {
    pub fn new() -> Self {
        Self {
            file: None,
            impure: false,
            args: Vec::new(),
            update_inputs: Vec::new(),
        }
    }

    pub fn file(mut self, file: impl AsRef<Path>) -> Self {
        self.file = Some(file.as_ref().to_owned());
        self
    }

    pub fn arg(mut self, name: &str, val: &str) -> Self {
        self.args.push((name.to_owned(), val.to_owned()));
        self
    }

    pub fn impure(mut self) -> Self {
        self.impure = true;
        self
    }

    pub fn update_input(mut self, input: &str) -> Self {
        self.update_inputs.push(input.to_owned());
        self
    }

    fn add_args(&self, cmd: &mut Command) {
        if let Some(file) = &self.file {
            cmd.arg("-f");
            cmd.arg(file);
        }
        if self.impure {
            cmd.arg("--impure");
        }
        for input in &self.update_inputs {
            cmd.arg("--update-input");
            cmd.arg(input);
        }
        for (name, value) in &self.args {
            cmd.arg("--argstr");
            cmd.arg(name);
            cmd.arg(value);
        }
    }

    pub fn eval<O: serde::de::DeserializeOwned>(self, target: &str) -> anyhow::Result<O> {
        let mut cmd = Command::new("nix");
        cmd.arg("eval");
        self.add_args(&mut cmd);
        cmd.arg("--json");
        cmd.arg(target);
        cmd.stdout(Stdio::piped());
        let mut child = cmd.spawn()?;
        let mut stdout = child.stdout.take().unwrap();
        let mut output = String::new();
        stdout.read_to_string(&mut output)?;
        let code = child.wait()?;
        if code.success() {
            let data: O = serde_json::from_str(&output)?;
            Ok(data)
        } else {
            Err(anyhow::anyhow!("nix exited with an error"))
        }
    }

    pub fn build(self, target: &str) -> anyhow::Result<PathBuf> {
        let mut cmd = Command::new("nix");
        cmd.arg("build");
        cmd.arg("--print-out-paths");
        cmd.arg("--no-link");
        self.add_args(&mut cmd);
        cmd.arg(target);
        cmd.stdout(Stdio::piped());
        let mut child = cmd.spawn()?;
        let mut stdout = child.stdout.take().unwrap();
        let mut output = String::new();
        stdout.read_to_string(&mut output)?;
        let code = child.wait()?;
        if code.success() {
            Ok(PathBuf::from(output.trim()))
        } else {
            Err(anyhow::anyhow!("nix exited with an error"))
        }
    }
}

pub fn add_gc_root(
    store_path: impl AsRef<Path>,
    root_path: impl AsRef<Path>,
) -> anyhow::Result<()> {
    let mut cmd = Command::new("nix-store");
    cmd.arg("--realise");
    cmd.arg("--add-root");
    cmd.arg(root_path.as_ref());
    cmd.arg(store_path.as_ref());
    let mut child = cmd.spawn()?;
    let code = child.wait()?;
    if !code.success() {
        return Err(anyhow::anyhow!("nix-store exited with an error"));
    }
    Ok(())
}
