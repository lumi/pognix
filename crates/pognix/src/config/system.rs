use std::{fs, io::ErrorKind, path::Path};

use serde::Deserialize;

const DEFAULT_CONFIG_PATH: &'static str = "/etc/pognix.toml";

#[derive(Debug, Clone, Deserialize)]
pub struct SystemSection {
    pub flake: String,
}

#[derive(Debug, Clone, Deserialize)]
pub struct SystemConfig {
    pub system: SystemSection,
}

impl SystemConfig {
    pub fn from_str(text: &str) -> anyhow::Result<SystemConfig> {
        Ok(toml::from_str(text)?)
    }

    pub fn from_file(path: impl AsRef<Path>) -> anyhow::Result<SystemConfig> {
        let text = fs::read_to_string(path)?;
        Self::from_str(&text)
    }

    pub fn from_global() -> anyhow::Result<SystemConfig> {
        let text = match fs::read_to_string(DEFAULT_CONFIG_PATH) {
            Ok(text) => text,
            Err(err) if err.kind() == ErrorKind::NotFound => {
                return Ok(SystemConfig::default());
            }
            Err(err) => {
                return Err(err.into());
            }
        };
        Self::from_str(&text)
    }
}

impl Default for SystemConfig {
    fn default() -> SystemConfig {
        SystemConfig {
            system: SystemSection {
                flake: "flake:nixos-config#devel".to_owned(),
            },
        }
    }
}
