use std::collections::HashMap;

use serde::{Deserialize, Serialize};

fn default_true() -> bool {
    true
}

/// Configuration for the container
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Container {
    /// User to use
    pub user: String,

    /// Uid of the user to use
    pub uid: u32,
}

/// Configuration for a volume
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Volume {
    /// Name of the volume
    pub name: String,

    /// Context this volume is in
    #[serde(default)]
    pub context: Option<String>,

    /// Destination to mount the volume
    pub dest: String, // PathBuf?
}

/// Configuration for exposing host functionality to the container
#[derive(Debug, Clone, Default, Serialize, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct Expose {
    /// Expose wayland to the container
    #[serde(default)]
    pub wayland: bool,

    /// Expose X11 to the container
    #[serde(default)]
    pub x11: bool,

    /// Expose pipewire to the container
    #[serde(default)]
    pub pipewire: bool,

    /// Expose pulseaudio to the container
    #[serde(default)]
    pub pulse: bool,

    /// Expose /dev/input to the container
    #[serde(default)]
    pub input: bool,

    /// Expose gpu to the container
    #[serde(default)]
    pub gpu: bool,

    /// Proxy xdg-open calls in the container to the host
    #[serde(default)]
    pub xdg_open: bool,
}

// TODO: have this not just be a string
/// Port mapping
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct PortMapping(pub String);

/// Network configuration for the container
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Network {
    /// Enable networking
    #[serde(default = "default_true")]
    pub enable: bool,

    /// Port mapping to expose
    #[serde(default)]
    pub mappings: Vec<PortMapping>,
}

impl Default for Network {
    fn default() -> Network {
        Network {
            enable: true,
            mappings: Vec::new(),
        }
    }
}

/// Full project container configuration
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ProjectConfig {
    /// Container configuration
    pub container: Container,

    /// Volume configurations
    #[serde(default)]
    pub volumes: HashMap<String, Volume>,

    /// Expose configuration
    #[serde(default)]
    pub expose: Expose,

    /// Network configuration
    #[serde(default)]
    pub network: Network,
}
