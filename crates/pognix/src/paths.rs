use std::{
    fs,
    path::{Path, PathBuf},
};

use crate::{util::PROJECT_NAME, xdg};

/// User paths on the host
#[derive(Debug, Clone)]
pub struct UserPaths {
    /// XDG_DATA_HOME
    pub data_dir: PathBuf,

    /// XDG_RUNTIME_DIR
    pub runtime_dir: PathBuf,

    /// The `"pognix"` directory under XDG_DATA_HOME
    pub pognix_data_dir: PathBuf,

    /// The `"pognix"` directory under XDG_RUNTIME_DIR
    pub pognix_runtime_dir: PathBuf,
}

impl UserPaths {
    /// Ensure that all the appropriate directories have been created, and then
    /// return the paths in a new `UserPaths`
    pub fn ensure() -> anyhow::Result<UserPaths> {
        let user_data_dir = xdg::data_home()?;
        let user_runtime_dir = xdg::runtime_dir();

        let pognix_data_dir = user_data_dir.join(PROJECT_NAME);
        let pognix_runtime_dir = user_runtime_dir.join(PROJECT_NAME);

        fs::create_dir_all(&pognix_data_dir)?;
        fs::create_dir_all(&pognix_runtime_dir)?;

        Ok(UserPaths {
            data_dir: user_data_dir,
            runtime_dir: user_runtime_dir,
            pognix_data_dir,
            pognix_runtime_dir,
        })
    }

    /// Ensure that all the appropriate project directories have been created,
    /// and then return the paths in a new `ProjectPaths`
    pub fn ensure_project(&self, name: &str) -> anyhow::Result<ProjectPaths> {
        let data_dir = self.pognix_data_dir.join("projects").join(name);
        let runtime_dir = self.pognix_runtime_dir.join("projects").join(name);

        fs::create_dir_all(&data_dir)?;
        fs::create_dir_all(&runtime_dir)?;

        Ok(ProjectPaths {
            data_dir,
            runtime_dir,
        })
    }

    /// Ensure a home directory exists for this user, and return it
    pub fn ensure_home(&self, name: &str) -> anyhow::Result<PathBuf> {
        let homedir_path = self.pognix_data_dir.join("home").join(name);
        fs::create_dir_all(&homedir_path)?;
        Ok(homedir_path)
    }

    /// Ensure a volume exists in this context, and return it
    pub fn ensure_volume(&self, context: &str, name: &str) -> anyhow::Result<PathBuf> {
        let volume_path = self
            .pognix_data_dir
            .join("volumes")
            .join(context)
            .join(name);
        fs::create_dir_all(&volume_path)?;
        Ok(volume_path)
    }
}

/// Project paths on the host
#[derive(Debug, Clone)]
pub struct ProjectPaths {
    /// The data directory for a project
    pub data_dir: PathBuf,

    /// The runtime directory for a project
    pub runtime_dir: PathBuf,
}

impl ProjectPaths {
    /// Ensure a root path for the container exists, and return it
    pub fn ensure_root(&self) -> anyhow::Result<PathBuf> {
        let root = self.runtime_dir.join("root");
        fs::create_dir_all(&root)?;
        Ok(root)
    }

    pub fn control_socket(&self) -> PathBuf {
        let control_socket = self.runtime_dir.join("control");
        let _ = fs::remove_file(&control_socket);
        control_socket
    }
}

#[derive(Debug, Clone)]
pub struct ContainerVolume {
    pub host: PathBuf,
    pub guest: PathBuf,
}

/// Host paths to locations inside the container
#[derive(Debug, Clone)]
pub struct ContainerPaths {
    /// The host path to the container root
    pub root: PathBuf,

    /// The host path to the home directory of the container user
    pub homedir: PathBuf,

    /// The host path to the top-level of the nixos system
    pub toplevel: PathBuf,

    /// The host path to the user XDG_RUNTIME_DIR
    pub runtime_dir: PathBuf,

    /// The host paths to volumes
    pub volumes: Vec<ContainerVolume>,

    /// The host path to the control socket
    pub control: PathBuf,
}

impl ContainerPaths {
    pub fn ensure(
        user_paths: &UserPaths,
        project_paths: &ProjectPaths,
        volumes: &[ContainerVolume],
        toplevel: impl AsRef<Path>,
        user: &str,
    ) -> anyhow::Result<ContainerPaths> {
        let root = project_paths.ensure_root()?;
        let homedir = user_paths.ensure_home(user)?;
        let runtime_dir = user_paths.runtime_dir.clone();
        let control = project_paths.control_socket();
        Ok(ContainerPaths {
            root,
            homedir,
            toplevel: toplevel.as_ref().to_owned(),
            runtime_dir,
            volumes: volumes.to_vec(),
            control,
        })
    }
}
