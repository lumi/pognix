use std::path::PathBuf;

use nix::unistd::{Uid, User};

/// Get current user home directory
pub fn home_dir() -> anyhow::Result<PathBuf> {
    std::env::var("HOME").map(PathBuf::from).or_else(|_| {
        let uid = Uid::current();
        let user = User::from_uid(uid)?.ok_or_else(|| anyhow::anyhow!("cannot find user"))?;
        Ok(user.dir)
    })
}

/// Get XDG_RUNTIME_DIR or fall back to `"/run/user/{uid}"`
pub fn runtime_dir() -> PathBuf {
    std::env::var("XDG_RUNTIME_DIR")
        .map(PathBuf::from)
        .unwrap_or_else(|_| {
            let uid = Uid::current();
            PathBuf::from("/run/user").join(uid.as_raw().to_string())
        })
}

/// Get XDG_DATA_HOME or fall back to `"~/.local/share"`
pub fn data_home() -> anyhow::Result<PathBuf> {
    std::env::var("XDG_DATA_HOME")
        .map(PathBuf::from)
        .or_else(|_| Ok(home_dir()?.join(".local/share")))
}
