use std::path::PathBuf;

use podman::{IdMap, RunBuilder};

use crate::{config::project::ProjectConfig, env::Env, paths::ContainerPaths};

/// Container environment parameters
#[derive(Debug, Clone)]
pub struct ContainerEnv {
    /// Host paths to locations inside the container
    pub paths: ContainerPaths,

    /// Project configuration
    pub config: ProjectConfig,

    /// Host env
    pub env: Env,
}

impl ContainerEnv {
    pub fn new(
        env: Env,
        config: ProjectConfig,
        paths: ContainerPaths,
    ) -> anyhow::Result<ContainerEnv> {
        Ok(ContainerEnv { env, paths, config })
    }

    pub fn apply(&self, builder: &mut RunBuilder) {
        self.apply_meta(builder);
        self.apply_tmpfs(builder);
        self.apply_users(builder);
        self.apply_nix(builder);
        self.apply_binds(builder);
        self.apply_expose(builder);
        self.apply_net(builder);
        self.apply_control(builder);

        builder.rootfs(&self.paths.root);
    }

    pub fn apply_meta(&self, builder: &mut RunBuilder) {
        builder.name(&self.env.name);
    }

    pub fn apply_tmpfs(&self, builder: &mut RunBuilder) {
        builder.tmpfs("/tmp");
        builder.tmpfs("/run");
        builder.tmpfs("/run/wrappers");
    }

    pub fn apply_users(&self, builder: &mut RunBuilder) {
        builder.uidmap(IdMap::passthrough(self.env.host_uid.as_raw()));
        builder.gidmap(IdMap::passthrough(self.env.host_gid.as_raw()));
    }

    pub fn apply_nix(&self, builder: &mut RunBuilder) {
        builder.bind_ro("/nix/store", "/nix/store");
        builder.bind_rw("/nix/var/nix/daemon-socket", "/nix/var/nix/daemon-socket");
    }

    pub fn apply_binds(&self, builder: &mut RunBuilder) {
        builder.bind_rw(
            &self.paths.homedir,
            PathBuf::from("/home").join(&self.config.container.user),
        );
        for volume in &self.paths.volumes {
            builder.bind_rw(&volume.host, &volume.guest);
        }
        builder.bind_rw(&self.env.project_root, &self.env.project_root);
    }

    pub fn apply_expose(&self, builder: &mut RunBuilder) {
        let expose = &self.config.expose;
        if expose.wayland {
            // TODO: pick wayland display more intelligently
            builder.bind_ro(
                self.paths.runtime_dir.join("wayland-1"),
                "/host/run/wayland-1",
            );
        }
        if expose.x11 {
            builder.bind_ro("/tmp/.X11-unix", "/tmp/.X11-unix");
        }
        if expose.pipewire {
            // TODO: pick pipewire socket more intelligently
            builder.bind_ro(
                self.paths.runtime_dir.join("pipewire-0"),
                "/host/run/pipewire-0",
            );
        }
        if expose.pulse {
            builder.bind_ro(
                self.paths.runtime_dir.join("pulse/native"),
                "/host/run/pulse/native",
            );
        }
        if expose.input {
            // TODO: bind_dev_ro?
            builder.bind_dev("/dev/input", "/dev/input");
        }
        if expose.gpu {
            // TODO: bind_dev_ro?
            builder.bind_dev("/dev/shm", "/dev/shm");
            builder.bind_dev("/dev/dri", "/dev/dri");
        }
    }

    pub fn apply_net(&self, builder: &mut RunBuilder) {
        let network = &self.config.network;
        if network.enable {
            // Set up port mappings
            for mapping in &network.mappings {
                builder.port_mapping(&mapping.0);
            }
        } else {
            builder.disable_net();
        }
    }

    pub fn apply_control(&self, builder: &mut RunBuilder) {
        builder.bind_rw(&self.paths.control, "/host/control");
    }
}
