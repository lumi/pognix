use std::{
    os::unix::ffi::OsStrExt,
    path::{Path, PathBuf},
};

use sha2::Digest;

/// Name of the project
pub const PROJECT_NAME: &'static str = "pognix";

/// Recurse parent directories to find one where there is a flake.nix
pub fn find_flake_root(path: impl AsRef<Path>) -> anyhow::Result<PathBuf> {
    let mut current_path = path.as_ref().to_owned();
    loop {
        let flake_path = current_path.join("flake.nix");
        // TODO: may wanna use try_exists?
        if flake_path.exists() {
            break Ok(current_path);
        }
        if !current_path.pop() {
            break Err(anyhow::anyhow!("flake not found"));
        }
    }
}

/// Hash a path into a container name
pub fn path_to_name(path: impl AsRef<Path>) -> String {
    let path = path.as_ref();
    let mut hasher = sha2::Sha256::new();
    hasher.update(&path.as_os_str().as_bytes());
    let data = hasher.finalize();
    format!(
        "{}-{}-{}",
        PROJECT_NAME,
        path.file_name().unwrap().to_string_lossy(),
        hex::encode(&data[0..4])
    )
}

/// Parse nixos system flake path
pub fn parse_system(text: &str) -> anyhow::Result<(&str, &str)> {
    let (system_path, system_attr) = text
        .split_once('#')
        .ok_or_else(|| anyhow::anyhow!("can't parse system flake path"))?;
    Ok((system_path, system_attr))
}
