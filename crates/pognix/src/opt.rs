use clap::Parser;

/// A podman and nix-based tool to manage development containers
#[derive(Debug, Clone, Parser)]
pub struct Opt {
    /// Override the system flake to use
    #[clap(short = 's', long = "system-flake")]
    pub system_flake: Option<String>,
    /// Override the project flake to use
    #[clap(short = 'f', long = "project-flake")]
    pub project_flake: Option<String>,
    /// Configuration file path
    #[clap(short = 'c', long = "config")]
    pub config: Option<String>,
    /// Override the name of the container
    #[clap(short = 'n', long = "name")]
    pub container: Option<String>,
    #[clap(subcommand)]
    pub command: Command,
}

#[derive(Debug, Clone, Parser)]
pub enum Command {
    /// Build the container
    Build {},
    /// Start the container
    Start {
        /// Don't detach when starting up
        #[clap(short = 'i', long = "interactive")]
        interactive: bool,
    },
    /// Stop the container
    Stop {},
    /// Enter into the container
    Enter {
        /// Start the container if it isn't running
        #[clap(short = 's', long = "start")]
        start: bool,
        /// Exit the container after this session closes
        #[clap(short = 'e', long = "stop-on-exit")]
        stop_on_exit: bool,
        /// User to enter the container as
        #[clap(short = 'u', long = "user")]
        user: Option<String>,
        /// Enter as root
        #[clap(short = 'r', long = "root")]
        root: bool,
    },
    /// Run a command in the container
    Run {
        /// User to run the command as
        #[clap(short = 'u', long = "user")]
        user: Option<String>,
        /// Run as root
        #[clap(short = 'r', long = "root")]
        root: bool,
        /// Command to run
        #[clap(multiple_values = true)]
        command: Vec<String>,
    },
    /// Clear built container
    Clear {},
    /// Print the container configuration
    PrintConfig {},
}
