{
  inputs = {};

  outputs = { ... }: {
    pognix.extraModules = [
      ({ pkgs, ... }: {
        hardware.opengl = {
          enable = true;
          driSupport = true;
        };

        pognix.expose = {
          wayland = true;
          x11 = true;
          pulse = true;
          pipewire = true;
          gpu = true;
          input = true;
        };
      })
    ];
  };
}
