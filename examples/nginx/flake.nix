{
  inputs = {};

  outputs = { ... }: {
    pognix.extraModules = [
      ({ pkgs, ... }: {
        pognix.network.mappings = [
          "8000:80"
        ];

        services.nginx = {
          enable = true;
          virtualHosts.default_server = {
            root = "/work";
          };
        };
      })
    ];
  };
}
